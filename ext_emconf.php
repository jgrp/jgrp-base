<?php

$EM_CONF[$_EXTKEY] = [
  'title' => 'TYPO3 Base Extension',
  'description' => 'Base extension for TYPO3 Setup',
  'category' => 'misc',
  'author' => 'Johannes',
  'author_email' => '',
  'state' => 'stable',
  'version' => '1.0.0',
  'constraints' => [
    'depends' => [
      'typo3' => '8.6.99',
    ],
  ],
];
