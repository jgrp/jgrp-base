lib {

  logo {
    // cat=lib/logo/1; type=file[gif,jpg,jpeg,png]; label=Logo image file
    file =
    // cat=lib/logo/3; type=string; label=Logo link
    link = 1
  }

  navigation {
    // cat=lib/navigation/1; type=int+; label=Main navigation root page
    main.rootPage = 1
    // cat=lib/navigation/2; type=int+; label=Footer navigation root page
    footer.rootPage = 7
  }

}
