styles {
  templates {
    templateRootPath = EXT:jgrp_base/Resources/Private/Temapltes/Extensions/Fluid_styled_content/
    partialRootPath =  EXT:jgrp_base/Resources/Private/Partials/Extensions/Fluid_styled_content/
    layoutRootPath =   EXT:jgrp_base/Resources/Private/Layouts/Extensions/Fluid_styled_content/
  }

  content.textmedia {
    maxW = 2000
    maxWInText = 450

    linkWrap.width = 1440m
    linkWrap.height = 900m

    linkWrap.lightboxEnabled = 1
  }

}
