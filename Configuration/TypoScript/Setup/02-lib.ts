lib {

  content = CONTENT
  content {
    table = tt_content
    select {
      where = colPos=0
      languageField = sys_language_uid
      orderBy = sorting
    }
  }

  # Example for logo
  # logo = IMAGE
  # logo {
  #   file = {$lib.logo.file}
  #   altText = {$lib.title}
  #   imageLinkWrap = 1
  #   imageLinkWrap {
  #     enable = 1
  #     typolink.parameter = {$lib.link}
  #   }
  # }

  # Example for footer-section
  # footer < lib.content
  # footer {
  #   select {
  #     where = colPos=1
  #   }
  #   slide = -1
  # }


  page {
    // Basic setup for Fluid template usage
    template = FLUIDTEMPLATE
    template {
      templateName {
        data = levelfield:-1, backend_layout_next_level, slide
        override.field = backend_layout
        ifEmpty = pagets__Main
        // Cut off leading "pagets__"
        substring = 8
      }

      layoutRootPaths {
        10 = EXT:jgrp_base/Resources/Private/Layouts/Page
      }

      partialRootPaths {
        10 = EXT:jgrp_base/Resources/Private/Partials/Page
      }

      templateRootPaths {
        10 = EXT:jgrp_base/Resources/Private/Templates/Page
      }

      variables {
        content =< lib.content
        # logo =< lib.logo
        # footer =< lib.footer

        mainnavigation_rootPage = TEXT
        mainnavigation_rootPage.value = {$lib.navigation.main.rootPage}

        footernavigation_rootPage = TEXT
        footernavigation_rootPage.value = {$lib.navigation.footer.rootPage}
      }

      # TODO: Remove after dropping support for TYPO3 6.2
      file.stdWrap.cObject = CASE
      file.stdWrap.cObject {
        // Prefer next level definition, fall back to own
        key {
          data = levelfield:-1, backend_layout_next_level, slide
          override.field = backend_layout
        }

        default = TEXT
        default.value = EXT:jgrp_base/Resources/Private/Templates/Page/Main.html
      }
    }
  }

}
