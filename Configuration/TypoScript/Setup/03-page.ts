page = PAGE
page {

  meta {
    description.field = description
    keywords.field = keywords
    author.field = author

    // og tags
    og:type = website
    og:type.attribute = property
    og:site_name = {$lib.title}
    og:site_name.attribute = property
    og:title.field = title
    og:title.attribute = property
    og:description.field = description
    og:description.attribute = property
    og:url < lib.link
    og:url.attribute = property

    // Responsive viewport
    viewport = width=device-width, initial-scale=1
  }

  headerData {
    1518436 = FLUIDTEMPLATE
    1518436 {
      file = EXT:jgrp_base/Resources/Private/Partials/Page/Assets/HrefLang-Links.html

      settings {
        language {
          0 = de
        }
      }
    }
  }

  10 =< lib.page.template
  10 {
    dataProcessing {
      10 = TYPO3\CMS\Frontend\DataProcessing\FilesProcessor
      10 {
        references.fieldName = tx_jgrpbase_hero_media
        as = heroMedia
      }
    }
  }
  
  extbase.controllerExtensionName = JgrpBase

  # includeCSS {
  #   main = EXT:jgrp_base/Resources/Public/Css/main.css
  # }

  # includeJSLibs {
  #   jquery = EXT:jgrp_base/Resources/Public/JavaScript/jquery/jquery.min.js
  # }

  # includeJS {
  #   main = EXT:jgrp_base/Resources/Public/JavaScript/main.js
  # }

}
