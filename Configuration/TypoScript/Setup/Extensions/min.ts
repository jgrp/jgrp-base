plugin.tx_min.tinysource {
  enable = 0

  head {
    stripTabs = 0
    stripNewLines = 0
    stripDoubleSpaces = 0
    stripTwoLinesToOne = 0
  }

  body {
	protectCode {
		10 = /(<script.*?>.*?<\/script>)/is
	}
  }
}


# Only enable for production
[applicationContext = Production*]
	plugin.tx_min.tinysource.enable = 1 
[global]
