lib.contentElement {
  settings {
    media {
      popup {
        linkParams.ATagParams.dataWrap =  title="Show" class="{$styles.content.textmedia.linkWrap.lightboxCssClass}" rel="{$styles.content.textmedia.linkWrap.lightboxRelAttribute}" data-width="{TSFE:lastImgResourceInfo|0}" data-height="{TSFE:lastImgResourceInfo|1}"
      }
    }
  }
}



tt_content {
  bullets {
    dataProcessing {
      30 = TYPO3\CMS\Frontend\DataProcessing\SplitProcessor
      30 {
        if {
          value = 3
          equals.field = bullets_type
        }
        fieldName = bodytext
        removeEmptyEntries = 1
        as = bullets
      }
    }
  }
}