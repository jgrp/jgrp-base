config {
  absRefPrefix = /

  noPageTitle = 2

  sys_language_overlay = 1

  language = de
  locale_all = de_DE.utf8
  sys_language_uid = 0

  compressCss = 1
  concatenateCss  = 1
  compressJs = 1
  concatenateJs = 1

  moveJsFromHeaderToFooter = 1

  tx_realurl_enable = 1

  headerComment (
    powered by jgrp
  )

  additionalHeaders {
    # only with existing SSL-Certificat
    #10.header = strict-transport-security:max-age=31536000
  }
}
