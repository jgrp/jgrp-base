TCEFORM {
  tt_content {
    CType {
      removeItems (
        text,
        textpic,
        image
      )
    }
    layout {
      # addItems {
      #    twocol = Bodytext as two columns
      # }
      removeItems = 2,3
      altLabels {
          1 = Bodytext as two columns
      #     2 = Box grün
      #     3 = Box rot
      }
    }
    frame_class {
      removeItems = ruler-before, ruler-after, indent
       addItems {
           header-left = Header Left
       }
    }

    bullets_type.addItems = 3
    bullets_type {
      addItems {
        3 = Chips
      }
    }
  }
}