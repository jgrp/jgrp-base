mod {
  web_layout {
    BackendLayouts {
      Main {
        title = Main

        config {
          backend_layout {
            colCount = 2
            rowCount = 2

            rows {
              1 {
                columns {
                  1 {
                    name = Content
                    colPos = 0
                    colspan = 2
                  }
                }
              }
            }
          }
        }
      }
    }
  }
}
