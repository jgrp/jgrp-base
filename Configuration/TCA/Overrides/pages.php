<?php
defined('TYPO3_MODE') or die();

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('pages', [
  'tx_jgrpbase_hero_media' => [
    'exclude' => true,
    'label' => 'LLL:EXT:jgrp_base/Resources/Private/Language/locallang_db.xlf:pages.tx_jgrpbase_hero_media',
    'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig('tx_jgrpbase_hero_media', [
      'maxitems' => 1,
      'appearance' => [
        'createNewRelationLinkTitle' => 'LLL:EXT:jgrp_base/Resources/Private/Language/locallang_db.xlf:pages.tx_jgrpbase_hero_media.selectMedia'
      ],
      'overrideChildTca' => [
        'columns' => [
          'uid_local' => [
            'config' => [
              'appearance' => [
                'elementBrowserAllowed' => implode(',', [
                  'gif',
                  'jpeg',
                  'jpg',
                  'png',
                  'mp4',
                  'webm',
                ]),
              ],
            ],
          ],
          'crop' => [
            'config' => [
              'cropVariants' => [
                'phone' => [
                  'title' => 'LLL:EXT:jgrp_base/Resources/Private/Language/locallang_db.xlf:imageManipulation.phone',
                  'allowedAspectRatios' => [
                    'Fixed' => [
                      'title' => 'LLL:EXT:jgrp_base/Resources/Private/Language/locallang_db.xlf:imageManipulation.aspectRatios.fixed',
                      'value' => 4 / 3,
                    ],
                  ],
                ],
                'tablet' => [
                  'title' => 'LLL:EXT:jgrp_base/Resources/Private/Language/locallang_db.xlf:imageManipulation.tablet',
                  'allowedAspectRatios' => [
                    'Fixed' => [
                      'title' => 'LLL:EXT:jgrp_base/Resources/Private/Language/locallang_db.xlf:imageManipulation.aspectRatios.fixed',
                      'value' => 6 / 4,
                    ],
                  ],
                ],
                'desktop' => [
                  'title' => 'LLL:EXT:jgrp_base/Resources/Private/Language/locallang_db.xlf:imageManipulation.desktop',
                  'allowedAspectRatios' => [
                    'Fixed' => [
                      'title' => 'LLL:EXT:jgrp_base/Resources/Private/Language/locallang_db.xlf:imageManipulation.aspectRatios.fixed',
                      'value' => 16 / 9,
                    ],
                  ],
                ],
              ],
            ],
          ],
        ],
        'types' => [
          \TYPO3\CMS\Core\Resource\File::FILETYPE_IMAGE => [
            'showitem' => implode(',', [
              'crop',
              '--palette--;;filePalette',
            ]),
          ],
        ],
      ],
    ]),
  ],
]);
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes('pages', 'tx_jgrpbase_hero_media', '', 'before:layout');