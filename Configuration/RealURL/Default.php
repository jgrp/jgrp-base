<?php

$GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['realurl']['_DEFAULT'] = [
    'init' => [
        'appendMissingSlash' => 'ifNotFile,redirect',
        'emptyUrlReturnValue' => '/',
    ],
    'pagePath' => [
        'rootpage_id' => 1,
    ],
    'fileName' => [
        'defaultToHTMLsuffixOnPrev' => false,
        'acceptHTMLsuffix' => false,
    ],
    'preVars' => [
        'language' => [
            'GETvar' => 'L',
            'noMatch' => 'bypass',
        ],
    ],
    'fixedPostVars' => [
        '_NEWS' => [
            'newsIdentifier' => [
                'GETvar' => 'tx_news_pi1[news]',
                'lookUpTable' => [
                    'table' => 'tx_news_domain_model_news',
                    'id_field' => 'uid',
                    'alias_field' => 'title',
                    'addWhereClause' => ' AND NOT deleted',
                    'useUniqueCache' => true,
                    'useUniqueCache_conf' => [
                        'strtolower' => true,
                        'spaceCharacter' => '-',
                    ],
                    'languageGetVar' => 'L',
                    'languageField' => 'sys_language_uid',
                    'transOrigPointerField' => 'l10n_parent',
                    'autoUpdate' => true,
                    'expireDays' => 180,
                ],
            ],
            'newsAction' => [
                'GETvar' => 'tx_news_pi1[action]',
                'noMatch' => 'bypass',
            ],
            'newsController' => [
                'GETvar' => 'tx_news_pi1[controller]',
                'noMatch' => 'bypass',
            ],
        ],
    ],
];
