<?php
defined('TYPO3_MODE') or die();

/* Add default RTE settings*/
$GLOBALS['TYPO3_CONF_VARS']['RTE']['Presets']['base_rte'] = 'EXT:jgrp_base/Configuration/RTE/base_rte.yaml';

$TYPO3_CONF_VARS['SYS']['locallangXMLOverride']['default']['EXT:form/Resources/Private/Language/locallang.xlf']['jgrp_base'] = 'EXT:jgrp_base/Resources/Private/Language/Extensions/Form/locallang.xlf';
$TYPO3_CONF_VARS['SYS']['locallangXMLOverride']['de']['EXT:form/Resources/Private/Language/locallang.xlf']['jgrp_base'] = 'EXT:jgrp_base/Resources/Private/Language/Extensions/Form/de.locallang.xlf';

/* Set fluid_template cache lifetime to one year */
$GLOBALS['TYPO3_CONF_VARS']['SYS']['caching']['cacheConfigurations']['fluid_template']['options']['defaultLifetime'] = 31536000;

$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['extbase']['commandControllers'][] = \Jgrp\JgrpBase\Command\SecurityCommandController::class;
