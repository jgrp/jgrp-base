<?php
namespace Jgrp\JgrpBase\Command;


use TYPO3\CMS\Core\Crypto\Random;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Mvc\Controller\CommandController;

/**
 * Command controller for security tasks
 */
class SecurityCommandController extends CommandController
{
  /**
   * Generate a new random encryption key
   *
   * @return void
   */
  public function generateEncryptionKeyCommand()
  {
    if (class_exists(Random::class)) {
      $key = GeneralUtility::makeInstance(Random::class)->generateRandomHexString(96);
    } else {
      $key = GeneralUtility::getRandomHexString(96);
    }
    
    $this->outputLine($key);
  }
}