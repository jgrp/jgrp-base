# jgrp base extension

Base extension for TYPO3 distributions following the DRY principle 

It covers a set of default settings and configuration for building a Typo3 website. Thus it includes a complete TypoScript and Backend configuration, as well as Frontend Resources.

- Material Design Lite Styling, Fluid - Templates and the Styling, which can be customized with constans
- Matching backend layouts
- Hero Images as content element
- Preconfigured RTE
- Rrealurl configuration
- typo3-form configuration for default contact form
- SEO Basic Setup (canonical Link, hrefLang links)
- Lazyload images
- Img elements as srcset
