define({
  index: 0 ,
  shareEl: false,
  bgOpacity: 0.4,
  closeOnScroll: false,
  counterEl: false,
  fullscreenEl: false,
  zoomEl: false,
  pinchToClose: false,
  maxSpreadZoom: 1,
  showAnimationDuration: 150,
  hideAnimationDuration: 150,
  getDoubleTapZoom: function(isMouseClick, item) {
    return item.initialZoomLevel;
  }
});