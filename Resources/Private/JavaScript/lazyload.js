require([
  "https://cdnjs.cloudflare.com/ajax/libs/vanilla-lazyload/8.7.1/lazyload.min.js"
], function( LazyLoad ) {

  new LazyLoad({
    container: document.getElementsByClassName("mdl-layout")[0]
  });
});