/**!
 * Lightbox
 * Photoswipe - photoswipe.com
 */


require([
  "photoswip-lib",
  "photoswipe-ui-lib",
  "photoswipe-options",
  "jquery",
], function( PhotoSwipe, PhotoSwipeUI_Default, pswpOptions, $ ) {




  $('.lightbox').click(function(e) {
    e.preventDefault();

    var images = [],
      $parentFrame =  $(this).closest('.frame'),
      frameId = $parentFrame.attr('id'),
      // position of image in gallery element
      index = $('#'+ frameId +' figure.image .lightbox').index(this),
      $element = $(this);

    // generate slides only from current text-media element
    $('#'+ frameId +' .lightbox').each(function() {
      images.push({
        src  : $(this).attr('href'),
        msrc : $(this).find('img').attr('src'),
        title: $(this).closest('figure.image').find('figcaption').text().trim(),
        w: $(this).data('width'),
        h: $(this).data('height')
      })
    })

    var pswpElement = document.querySelectorAll('.pswp')[0];
    $.extend(pswpOptions, {
      index: index,
      galleryUID: frameId.substring(1),
      getThumbBoundsFn: function(index) {
        var thumbnail = $('#'+ frameId +' figure.image .lightbox').get(index), // find thumbnail
          pageYScroll = window.pageYOffset || document.documentElement.scrollTop,
          rect = thumbnail.getBoundingClientRect();
        return {x:rect.left, y:rect.top + pageYScroll, w:rect.width};
      },
    })

    // Initializes and opens PhotoSwipe
    var gallery = new PhotoSwipe( pswpElement, PhotoSwipeUI_Default, images, pswpOptions);
    gallery.init();
    gallery.listen('initialZoomIn', function() {
      $(".mdl-layout").addClass('blurred-content');
    });
    gallery.listen('initialZoomOut', function()  {
      $(".mdl-layout").removeClass('blurred-content');
    })
  })


});